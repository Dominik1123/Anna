import tempfile
from pathlib import Path
from string import ascii_lowercase
from tempfile import TemporaryDirectory
import textwrap
from typing import Sequence

import numpy as np
import pandas as pd

from anna.sweeps import (
    Combinator, ProductCombination, ZipCombination, NumberRange, IntegerRange, FilepathRange, Distribution,
    Generator, Generate, Sweep, Transformation, Linear, Log10,
)


def test_number_range():
    assert len(NumberRange(0, 1, 10, Distribution.GRID, Linear).generate()) == 10
    assert len(NumberRange(1, 10, 10, Distribution.RANDOM, Log10).generate()) == 10


def test_integer_range():
    assert np.array_equal(
        IntegerRange(1, 10, 10, Distribution.GRID, Linear).generate(),
        list(range(1, 11)),
    )


def test_filepath_range():
    with TemporaryDirectory() as td:
        (Path(td) / 'foo').touch()
        (Path(td) / 'bar').touch()
        (Path(td) / 'baz').touch()
        assert len(FilepathRange(td, '*').generate()) == 3
        assert len(FilepathRange(td, 'ba*').generate()) == 2
        assert len(FilepathRange(td, 'f*').generate()) == 1


def test_combinator():
    parameters = [
        NumberRange(0, 1, 2, Distribution.GRID, Linear),
        NumberRange(0, 1, 3, Distribution.GRID, Linear),
        NumberRange(0, 1, 4, Distribution.GRID, Linear),
    ]
    assert len(Combinator(parameters, method=ProductCombination).generate()) == 2*3*4
    assert len(Combinator(parameters, method=ZipCombination).generate()) == 2


def test_combinator_product_combination():
    parameters = [
        GeneratorMock([1, 2, 3]),
        GeneratorMock([4, 5]),
        GeneratorMock([6, 7, 8, 9]),
    ]
    assert np.array_equal(
        Combinator(parameters, method=ProductCombination).generate(),
        [
            [1, 4, 6],
            [1, 4, 7],
            [1, 4, 8],
            [1, 4, 9],
            [1, 5, 6],
            [1, 5, 7],
            [1, 5, 8],
            [1, 5, 9],

            [2, 4, 6],
            [2, 4, 7],
            [2, 4, 8],
            [2, 4, 9],
            [2, 5, 6],
            [2, 5, 7],
            [2, 5, 8],
            [2, 5, 9],

            [3, 4, 6],
            [3, 4, 7],
            [3, 4, 8],
            [3, 4, 9],
            [3, 5, 6],
            [3, 5, 7],
            [3, 5, 8],
            [3, 5, 9],
        ],
    )


def test_combinator_zip_combination():
    parameters = [
        GeneratorMock([1, 2, 3]),
        GeneratorMock([4, 5]),
        GeneratorMock([6, 7, 8, 9]),
    ]
    assert np.array_equal(
        Combinator(parameters, method=ZipCombination).generate(),
        [[1, 4, 6], [2, 5, 7]],
    )


def test_sweep():
    parameters = [
        GeneratorMock([1, 2, 3, 4]),
        Combinator([
            GeneratorMock(['a', 'b']),
            GeneratorMock(['x', 'y']),
        ], method=ProductCombination),
    ]
    names = ['first', 'second']
    df = Sweep(names, Combinator(parameters, method=ZipCombination)).to_dataframe()
    df['second'] = df['second'].apply(np.ndarray.tolist)
    assert df.to_dict(orient='index') == {
        0: dict(first=1, second=['a', 'x']),
        1: dict(first=2, second=['a', 'y']),
        2: dict(first=3, second=['b', 'x']),
        3: dict(first=4, second=['b', 'y']),
    }


def test_generate_sweep_files():
    sweep = Sweep(
        names=['path/to/first', 'path/to/second'],
        combinator=Combinator(
            [GeneratorMock(list(range(3))), GeneratorMock(list(ascii_lowercase))],
            method=ProductCombination,
        ),
    )
    with TemporaryDirectory() as td:
        td = Path(td)
        seed_path = td / 'seed.xml'
        # Parameter 'second' is missing on purpose (--> should be inserted).
        seed_path.write_text(textwrap.dedent(
            '''\
            <?xml version="1.0" ?>
            <root>
                <path>
                    <to>
                        <first>first</first>
                    </to>
                </path>
            </root>
            '''
        ))
        generate = Generate(
            seed_path=seed_path,
            folder_path=td,
            config_prefix='',
            sweep_instance=sweep,
            meta={},
            constants={'path/to/third': 'this_is_a_constant'},
        )
        generate.all()
        assert td.joinpath(Generate.CONFIG_DIRECTORY).is_dir()
        assert sorted(p.name for p in td.joinpath(Generate.CONFIG_DIRECTORY).glob('*')) == [f'{i:02d}.xml' for i in range(78)]
        assert td.joinpath('names_to_txt.txt').is_file()
        assert td.joinpath('data.csv').is_file()
        data = pd.read_csv(td.joinpath('data.csv'), index_col=0)
        assert (data.columns == ['Configuration filename', 'path/to/first', 'path/to/second' ,'path/to/third']).all()
        assert len(data) == 78
        assert (data['path/to/first'] == [0]*26 + [1]*26 + [2]*26).all()
        assert (data['path/to/second'] == list(ascii_lowercase)*3).all()
        assert (data['path/to/third'] == 'this_is_a_constant').all()
        assert '<first>0</first>' in td.joinpath(Generate.CONFIG_DIRECTORY).joinpath('00.xml').read_text()
        assert '<second>a</second>' in td.joinpath(Generate.CONFIG_DIRECTORY).joinpath('00.xml').read_text()
        assert '<first>2</first>' in td.joinpath(Generate.CONFIG_DIRECTORY).joinpath('77.xml').read_text()
        assert '<second>z</second>' in td.joinpath(Generate.CONFIG_DIRECTORY).joinpath('77.xml').read_text()
        seed_config_lines = open(seed_path).readlines()[:8]
        del seed_config_lines[4]
        test_config_lines = open(td.joinpath(Generate.CONFIG_DIRECTORY).joinpath('00.xml')).readlines()[:9]
        del test_config_lines[4:6]  # Also delete the inserted <second> parameter.
        assert seed_config_lines == test_config_lines


class GeneratorMock(Generator):
    def __init__(self, sequence: Sequence):
        self.sequence = sequence

    @property
    def count(self) -> int:
        return len(self.sequence)

    def generate(self) -> Sequence:
        return self.sequence
