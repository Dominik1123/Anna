import sys

from anna.frontends.qt.sweeps import find_widget_for_parameter, SweepWidget
from anna.parameters import VectorParameter, NumberParameter, FilepathParameter

from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QVBoxLayout
from PyQt5.QtCore import QCoreApplication, Qt


if __name__ == '__main__':
    QCoreApplication.setAttribute(Qt.AA_X11InitThreads)
    app = QApplication(sys.argv)
    main_window = QMainWindow()
    main_window.setCentralWidget(SweepWidget([
        find_widget_for_parameter(NumberParameter('Number'), 1.0),
        find_widget_for_parameter(FilepathParameter('File'), '/tmp/test'),
        find_widget_for_parameter(VectorParameter[NumberParameter]('TransverseOffset', n=2), [0, 0]),
    ]))
    main_window.show()
    app.exec_()
