import sys

from anna.frontends.qt.sweeps import find_widget_for_parameter
from anna.parameters import VectorParameter, NumberParameter

from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QVBoxLayout
from PyQt5.QtCore import QCoreApplication, Qt


class MainWidget(QWidget):
    def __init__(self):
        super().__init__()
        layout = QVBoxLayout()
        layout.addWidget(find_widget_for_parameter(VectorParameter[NumberParameter]('test', n=3), [1, 2, 3]))
        self.setLayout(layout)


if __name__ == '__main__':
    QCoreApplication.setAttribute(Qt.AA_X11InitThreads)
    app = QApplication(sys.argv)
    main_window = QMainWindow()
    main_window.setCentralWidget(MainWidget())
    main_window.show()
    app.exec_()
