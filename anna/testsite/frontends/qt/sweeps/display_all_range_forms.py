import inspect
import sys
from typing import List, Type

import anna.frontends.qt.sweeps
from anna.frontends.qt.sweeps import ParameterRangeForm, ParameterRangeWidget

from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QVBoxLayout
from PyQt5.QtCore import QCoreApplication, Qt


FORMS : List[Type[ParameterRangeForm]] = [
    c for c in vars(anna.frontends.qt.sweeps).values()
    if inspect.isclass(c) and issubclass(c, ParameterRangeForm) and c is not ParameterRangeForm
]


class MainWidget(QWidget):
    def __init__(self):
        super().__init__()
        layout = QVBoxLayout()
        for i, Form in enumerate(FORMS):
            parameter = Form.PEER(Form.__name__)
            layout.addWidget(ParameterRangeWidget(
                parameter.name,
                parameter.convert_representation(str(i), {}),
                Form(),
            ))
        self.setLayout(layout)


if __name__ == '__main__':
    QCoreApplication.setAttribute(Qt.AA_X11InitThreads)
    app = QApplication(sys.argv)
    main_window = QMainWindow()
    main_window.setCentralWidget(MainWidget())
    main_window.show()
    app.exec_()
