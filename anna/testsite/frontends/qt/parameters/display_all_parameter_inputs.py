from functools import partial
import inspect
import sys
from typing import List, Type

from anna.frontends.qt.parameters import from_type
import anna.parameters as parameters

from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QHBoxLayout, QVBoxLayout, QPushButton, QInputDialog
from PyQt5.QtCore import QCoreApplication, Qt


PARAMETERS = [
    parameters.BoolParameter,
    parameters.IntegerParameter,
    parameters.StringParameter,
    parameters.FilepathParameter,
    parameters.NumberParameter,
    partial(parameters.PhysicalQuantityParameter, unit='m'),
    partial(parameters.VectorParameter[parameters.PhysicalQuantityParameter], unit='s'),
    parameters.VectorParameter[parameters.FilepathParameter],
    parameters.TripletParameter[parameters.FilepathParameter],
]


class MainWidget(QWidget):
    def __init__(self):
        super().__init__()
        layout = QVBoxLayout()
        for i, Parameter in enumerate(PARAMETERS):
            name = Parameter.func.__name__ if isinstance(Parameter, partial) else Parameter.__name__
            p_input = from_type(Parameter(name))
            print_button = QPushButton('print')
            print_button.clicked.connect(lambda _, p=p_input: print(p.text))
            set_button = QPushButton('set')
            set_button.clicked.connect(lambda _, p=p_input: setattr(p, 'text', QInputDialog.getText(self, 'Enter text', 'Text:')[0]))
            h_layout = QHBoxLayout()
            h_layout.addWidget(print_button)
            h_layout.addWidget(set_button)
            h_layout.addWidget(p_input)
            layout.addLayout(h_layout)
        self.setLayout(layout)


if __name__ == '__main__':
    QCoreApplication.setAttribute(Qt.AA_X11InitThreads)
    app = QApplication(sys.argv)
    main_window = QMainWindow()
    main_window.setCentralWidget(MainWidget())
    main_window.show()
    app.exec_()
