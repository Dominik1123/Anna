#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, unicode_literals

import sys

from anna.adaptors import load_from_file
from anna.frontends.qt.parameters import ComplementaryGroupInput
from anna.parameters import IntegerParameter, PhysicalQuantityParameter, \
    ComplementaryParameterGroup
# noinspection PyPackageRequirements
from PyQt4 import QtCore, QtGui
import six


if __name__ == '__main__':
    def sum_all(*args):
        return int(sum(args))

    group = ComplementaryParameterGroup(
        'TestGroup',
        (
            IntegerParameter('Integer1'),
            sum_all
        ),
        (
            IntegerParameter('Integer2'),
            sum_all
        ),
        (
            IntegerParameter('Integer3'),
            sum_all
        ),
        (
            PhysicalQuantityParameter('PhysicalQuantity', unit='m'),
            sum_all
        ),
        info='This is a test complementary group.'
    )

    QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_X11InitThreads)
    app = QtGui.QApplication(sys.argv)

    widget = QtGui.QWidget()
    form = ComplementaryGroupInput(group)

    load_from_source_button = QtGui.QPushButton('load')

    def load_from_source():
        filepath = QtGui.QFileDialog.getOpenFileName()
        form.load_from_source(load_from_file(six.text_type(filepath)))

    # noinspection PyUnresolvedReferences
    load_from_source_button.clicked.connect(load_from_source)

    dump_as_json_button = QtGui.QPushButton('json')

    def dump_as_json():
        print(form.as_config('json'))

    # noinspection PyUnresolvedReferences
    dump_as_json_button.clicked.connect(dump_as_json)

    dump_as_xml_button = QtGui.QPushButton('xml')

    def dump_as_xml():
        print(form.as_config('xml'))

    # noinspection PyUnresolvedReferences
    dump_as_xml_button.clicked.connect(dump_as_xml)

    v_layout = QtGui.QVBoxLayout()
    v_layout.addWidget(form)

    h_layout = QtGui.QHBoxLayout()
    h_layout.addWidget(load_from_source_button)
    h_layout.addStretch(1)
    h_layout.addWidget(dump_as_json_button)
    h_layout.addWidget(dump_as_xml_button)
    v_layout.addLayout(h_layout)

    widget.setLayout(v_layout)

    widget.show()

    sys.exit(app.exec_())
